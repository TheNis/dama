public class Pedina {
    protected int x, y;
    protected boolean colore;
    Pedina(int x, int y)
        {this.x=x;this.y=y;}

    public int getX()
        {return x;}

    public int getY()
        {return y;}

    public int[] muovi(Direzione d)
        {if(colore)
            {if (d == Direzione.NORDEST)
                {x++; y++;}
            if(d == Direzione.NORDOVEST)
                {x--; y++;}}
        else
            {if(d== Direzione.SUDEST)
                {x++; y--;}
            if(d== Direzione.SUDOVEST)
                {x--; y--;}}
        int[] t= {x, y};
        return t;}

    public int[] mangia(Direzione d, Pedina p)
        {if(colore && !(p instanceof Dama))
            {if (d == Direzione.NORDEST)
                {x++; x++; y++; y++;}
            if(d == Direzione.NORDOVEST)
                {x--; x--; y++; y++;}}
        else if(!colore && !(p instanceof Dama))
            {if(d== Direzione.SUDEST)
                {x++; x++; y--; y--;}
            if(d== Direzione.SUDOVEST)
                {x--; x--; y--; y--;}}
        int[] t= {x, y};
        return t;}




}
