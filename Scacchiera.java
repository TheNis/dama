import java.util.ArrayList;
import java.util.Arrays;

public class Scacchiera {
    private ArrayList<Pedina> bianchi = new ArrayList<Pedina>();
    private ArrayList<Pedina> neri = new ArrayList<Pedina>();
    private boolean[][] casella=new boolean[8][8];

    public Scacchiera()
        {for(int i=0; i<3; i++)
            {int j=0;
                if(i==0 || i==2)
                    j=1-2;
                for(; j<8; j=j+2)
                    {bianchi.add(new Pedina(j, i));
                    casella[j][i]=true;}}

        for(int i=5; i<8; i++)
            {int j=0;
                if(i==1)
                    j=1-2;
                for(; j<8; j=j+2)
                    {neri.add(new Pedina(j, i));
                    casella[j][i]=true;}}

        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                if(!casella[i][j])
                    casella[i][j]=false;}

    public int getBianchiRimasti() {return bianchi.size();}

    public int getNeriRimasti() {return neri.size();}

    public String toString() {
        return "Scacchiera{" +
                "bianchi=" + bianchi +
                ", neri=" + neri +
                ", casella=" + Arrays.toString(casella) +
                '}';
    }

    public void muovi(Direzione d) {}
}
