public class Gioco {
    private boolean turno;
    private int contTurni;
    private Scacchiera scacchiera;

    public Gioco() {this.turno=turno; this.contTurni=contTurni;}

    public void EseguiTurno() {

        turno=!turno;
        contTurni++;
    }

    public char vincitore() {
        if(scacchiera.getBianchiRimasti()==0) return 'N';
        if(scacchiera.getNeriRimasti()==0) return 'B';
        return 'A';
    }
}
